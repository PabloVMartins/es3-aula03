
const express = require('express');

const app = express();

app.use(express.json());

let clients = [
  {id: 3, nome: "Pablo Martins", teledfone: "53981001122"},
  {id: 1, nome: "Lisie Martins", teledfone: "53981223344"},
  {id: 2, nome: "Oliver Martins", teledfone: "53981334455"},
  {id: 4, nome: "Helena Martins", teledfone: "53981445566"},
]

function log (request, response, next) {
  const {url, method} = request;
  console.log(`${method} - ${url} at ${new Date()}`);

  return next();
}

app.use(log);

app.get('/clients', (request, response) => response.json(clients))

app.get('/clients/:id', (request, response) => {
  const { id } = request.params;
  const client = clients.find(value => value.id == id);

  if (client == undefined) {
    response.status(400).json({ error: "Requisição inválida" });
  } else {
    response.status(200).json(client);
  }
})

app.post('/clients', (request, response) => {
  const client = request.body;
  clients.push(client);
  response.status(201).json(client);
})

app.put('/clients/:id', (request, response) => {
  const id = request.params.id;
  const { nome } = request.body;

  let client = clients.find(value => value.id == id)

  if (client == undefined) {
    response.status(400).json({ error: "Requisição inválida" })
  } else {
    client.nome = nome;
  
    response.status(200).json(client)
  }
})

app.delete('/clients/:id', (request, response) => {
  const { id } = request.params;
  const index = clients.findIndex(value => value.id == id);

  if(index == -1) {
    response.status(400).json({ error: "Requisição inválida" });
  } else {
    clients.splice(index, 1);
    
    response.status(204).send();
  }
})

app.listen(3000);